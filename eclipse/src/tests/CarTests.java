package tests;
import vehicles.Car;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void testGetters() {
		Car c=new Car(5);
		
		assertEquals(5,c.getSpeed());
		assertEquals(0,c.getLocation());
	}
	@Test
	void testMoving() {
		Car c=new Car(5);
		c.moveRight();
		c.moveRight();
		assertEquals(10,c.getLocation());
		
		c.moveLeft();
		assertEquals(5,c.getLocation());
	}
	@Test
	void testAccelerationDeceleration() {
		Car c=new Car(5);
		c.accelerate();
		c.moveLeft();
		assertEquals(6,c.getSpeed());
		assertEquals(-6,c.getLocation());
		
		c.decelerate();
		c.moveRight();
		assertEquals(5,c.getSpeed());
		assertEquals(-1,c.getLocation());
	}
	@Test
	void testDecelerationAtZero() {
		Car c=new Car(0);
		c.decelerate();
		assertEquals(0,c.getSpeed());
	}
	

}
