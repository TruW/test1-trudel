package tests;
import utilities.MatrixMethod;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void tMatrixMethod() {
		MatrixMethod m=new MatrixMethod();
		int[][] origin= {{1,2,3},
						{4,5,6}};
		int[][] dupeEx= {{1,2,3,1,2,3},
						{4,5,6,4,5,6}};
		int[][] dupe=m.duplicate(origin);
		
		for(int i=0;i>dupe.length;i++) {
			for(int j=0;j>dupe[i].length;i++) { 
				assertEquals(dupeEx[i][j],dupe[i][j]);
		}
	}
 }

}
