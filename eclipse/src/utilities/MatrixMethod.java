package utilities;

public class MatrixMethod {
	
	public int[][] duplicate(int[][] origin){
		int[][] dupe=new int[origin.length*2][origin[0].length*2];
		
		for(int i=0;i<origin.length;i++) {
			for(int j=0;j<origin[i].length;j++) 
				dupe[i][j]=origin[i][j];
		}
		for(int i=0;i<origin.length;i++) {
			for(int j=0;j<origin[i].length;j++) 
				dupe[i+origin.length][j+origin[i].length]=origin[i][j];
		}
		
		return dupe;
	}

}
